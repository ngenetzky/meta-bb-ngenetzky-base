# Copyright (C) 2018 Nathan Genetzky <nathan@genetzky.us>
# Released under the MIT license (see COPYING.MIT for the terms)

inherit bb_fetcher
addtask do_unpack before do_build

SRCREV = "66bc81c37bd882cf3e6fa42c29f4984698cfd172"
SRC_URI = "git://github.com/crops/poky-container.git"
S = "${WORKDIR}/git"

inherit docker

console(){
    local name="${PN}"
    docker run -it \
        --rm \
        --name "${name}" \
        --volume "${@docker_volume_path(d,'poky')}:/workspace" \
        --volume "${TOPDIR}:/mnt/topdir:ro" \
        "${DOCKER_REPOSITORY}:${DOCKER_TAG}" \
        --workdir="/workspace/"
}

inherit bb_build_shell
do_build_shell_scripts[nostamp] = "1"
addtask do_build_shell_scripts before do_build
python do_build_shell_scripts(){
    workdir = d.getVar('WORKDIR', expand=True)
    export_func_shell('console', d, os.path.join(workdir, 'console.sh'), workdir)
}

do_build[dirs] = "${S}"
do_build(){
    docker build \
        --build-arg BASE_DISTRO="ubuntu-18.04" \
        -t "${DOCKER_REPOSITORY}:${DOCKER_TAG}" \
        .
    docker image inspect \
        "${DOCKER_REPOSITORY}:${DOCKER_TAG}"
}
