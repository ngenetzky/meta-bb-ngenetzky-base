# Copyright (C) 2020 Nathan Genetzky <nathan@genetzky.us>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A minimal Ubuntu base image modified for Docker-friendliness"
HOMEPAGE = "https://github.com/phusion/baseimage-docker"
LICENSE = "MIT"
SECTION = "dockerbake"

PR = "r0"

inherit bb_fetcher
addtask do_unpack before do_build

SRC_URI = "\
    file://Dockerfile;subdir=${PN}-${PV} \
"

# https://hub.docker.com/layers/phusion/baseimage/bionic-1.0.0/images/sha256-0481bb9b387564c43bd5d44cacf44efb492fd1e7bd1716e849262036b69c030c?context=explore
# phusion/baseimage:bionic-1.0.0
# Digest:sha256:0481bb9b387564c43bd5d44cacf44efb492fd1e7bd1716e849262036b69c030c
DOCKER_FROM_IMAGE ?= "phusion/baseimage"
DOCKER_FROM_TAG ?= "bionic-1.0.0"
DOCKER_FROM_DIGEST ?= "sha256:0481bb9b387564c43bd5d44cacf44efb492fd1e7bd1716e849262036b69c030c"
DOCKER_FROM ??= "${DOCKER_FROM_IMAGE}:${DOCKER_FROM_TAG}@${DOCKER_FROM_DIGEST}"

# TODO: Need to standardize on the names of these.
DOCKER_IMAGE_PREFIX ?= "bb/"
DOCKER_IMAGE_NAME ?= "${PN}"
DOCKER_REPOSITORY ?= "${DOCKER_IMAGE_PREFIX}${DOCKER_IMAGE_NAME}"
DOCKER_TAG ?= "${PV}-${PR}"

do_build[dirs] = "${B}"
do_build(){
    # IMAGE_ROOTFS # TODO Copy in?
    mkdir -p 'rootfs/'
    find 'rootfs/'

    _build_from="${DOCKER_FROM}"
    _build_arch="amd64"
    _build_date="$(date +'%Y-%m-%dT%H:%M:%SZ')"
    _build_ref="453f835df0569224ad421d5d1733031c8e171619"
    _build_version="${PF}"

    _docker_repository="${DOCKER_REPOSITORY}"
    _docker_tag="${DOCKER_TAG}"

    docker pull \
        "${DOCKER_FROM_IMAGE}:${DOCKER_FROM_TAG}"

    docker build \
        --build-arg "BUILD_FROM=${_build_from?}" \
        --build-arg "BUILD_DATE=${_build_date?}" \
        --build-arg "BUILD_ARCH=${_build_arch?}" \
        --build-arg "BUILD_REF=${_build_ref?}" \
        --build-arg "BUILD_VERSION=${_build_version?}" \
        --tag "${_docker_repository?}:${_docker_tag?}" \
        './'

    docker image inspect \
        "${_docker_repository}:${_docker_tag}"
}
