# Copyright (C) 2020 Nathan Genetzky <nathan@genetzky.us>
# Released under the MIT license (see COPYING.MIT for the terms)
require kas-poky.inc

PV = "3.1.1"
PR = "${INC_PR}.0"

SRC_URI = "\
    file://kas.yml \
"
